#!/bin/bash

NAME="food_project"                                  # Name of the application
DJANGODIR=/var/www/src/                 # Django project directory
SOCKFILE=/var/www/run/gunicorn.sock     # we will communicte using this unix socket
USER=root                                        # the user to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=food2office.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=food2office.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /var/www/venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

exec /var/www/venv/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER  \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-


