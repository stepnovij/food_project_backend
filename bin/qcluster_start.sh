#!/bin/bash

NAME="food_project"                                  # Name of the application
DJANGODIR=/var/www/src/                 # Django project directory
USER=root                                        # the user to run as
DJANGO_SETTINGS_MODULE=food2office.settings             # which settings file should Django use

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /var/www/venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

exec /var/www/src/manage.py qcluster