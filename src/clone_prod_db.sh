if [ ! -f food2office_$(date +%Y%m%d).sql ];then
    ssh root@62.109.14.234 'PGPASSWORD="super_test_1234" pg_dump --host 127.0.0.1  --port 5432 --user food2office_user --no-owner -d food2office > food2office_$(date +%Y%m%d).sql'
    scp root@62.109.14.234:food2office_$(date +%Y%m%d).sql .
fi
psql -d postgres -c 'drop database food2office'
psql -d postgres -c 'create database food2office with owner pgsql'
psql --user pgsql -d food2office < food2office_$(date +%Y%m%d).sql
psql -d food2office -c 'GRANT USAGE ON SCHEMA public to food2office_user'
psql -d food2office -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO food2office_user;"
psql -d food2office -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO food2office_user;"
psql -d postgres -c 'ALTER USER food2office_user CREATEDB'
psql -d food2office -c "ALTER TABLE public.menu_ordercourse OWNER TO food2office_user"
psql -d food2office -c "ALTER TABLE public.menu_order OWNER TO food2office_user"
psql -d food2office -c "ALTER TABLE public.menu_orderpayment OWNER TO food2office_user"
psql -d food2office -c "ALTER TABLE public.menu_usermoneypayment OWNER TO food2office_user"
psql -d food2office -c "ALTER TABLE public.menu_usermoneybalance OWNER TO food2office_user"