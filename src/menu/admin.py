from django.conf.urls import url
from django.contrib import admin
from django.template.response import TemplateResponse
from menu.models import Course, OrderUser
from menu.models.course import Provider
from menu.models.order import Order, UserMoneyPayment, UserMoneyBalance
from menu.models.user import Company


class CourseAdmin(admin.ModelAdmin):
    list_display = ('week', 'date', 'name', 'weight', 'price')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('week', 'user', 'is_payed', 'total_sum')
    readonly_fields = ('total_sum',)

    def total_sum(self, obj):
        return sum(
            order_course.quantity*order_course.course.price
            for order_course in obj.ordercourse_set.all()
        )


class OrderGenerateXLS(admin.ModelAdmin):
    def get_urls(self):
        urls = super(OrderGenerateXLS, self).get_urls()
        my_urls = [
            url(r'^my_view/$', self.admin_site.admin_view(self.my_view)),
        ]
        return my_urls + urls

    def my_view(self, request):
        context = dict()
        return TemplateResponse(request, "admin/menu/extended_base_site.html", context)


admin.site.index_template = 'admin/custom_index.html'
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderUser)
admin.site.register(Company)
admin.site.register(Provider)
admin.site.register(UserMoneyPayment)
admin.site.register(UserMoneyBalance)
admin.site.register(Course, CourseAdmin)
admin.autodiscover()
