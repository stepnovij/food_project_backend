from datetime import date, datetime, time, timedelta

from django.contrib.auth import get_user_model
from menu.models import Course
from menu.models.course import Provider
from menu.serializers import CourseSerializer, ProviderSerializer
from rest_framework import filters, views, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

User = get_user_model()

DAY_ORDER_DEADLINE = time(11,50)


def today_or_next_day_order():
    if date.today().weekday() >= 4:
        if date.today().weekday() == 4 and datetime.now().time() > DAY_ORDER_DEADLINE:
            return date.today() + timedelta(days=4)
        elif date.today().weekday() == 4 and datetime.now().time() < DAY_ORDER_DEADLINE:
            return date.today() + timedelta(days=2)
        elif date.today().weekday() > 4:
            return date.today() + timedelta(days=4 - (date.today().weekday() - 4))
    else:
        if datetime.now().time() > DAY_ORDER_DEADLINE:
            return date.today() + timedelta(days=2)
        return date.today() + timedelta(days=1)


class ProviderViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    filter_backends = (filters.DjangoFilterBackend,)


class CourseViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        week = self.request.query_params.get('week', None)
        queryset = self.queryset.filter(
            date__gte=today_or_next_day_order(),
            week__number__gte=date.today().isocalendar()[1]
        ).order_by('date', 'course_type_order')
        if week == 'next':
            return queryset.filter(
                week__number=(date.today().isocalendar()[1]+1))
        return queryset.filter(week__number=(date.today().isocalendar()[1]))


class WeekView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        next_week = None
        if Course.objects.filter(
            week__number=date.today().isocalendar()[1] + 1
        ).exists():
            next_week = date.today().isocalendar()[1] + 1
        return Response({
            'current_week': date.today().isocalendar()[1],
            'next_week': next_week
        })
