import os

from datetime import date, timedelta

from rest_framework import filters, views, viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from django.db.models import Sum
from django.contrib.postgres.aggregates import ArrayAgg
from django.http import HttpResponse

from menu.models import Course, Order, OrderCourse
from menu.serializers import OrderSerializer, SimpleOrderCourseSerializer


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user).order_by('week__number')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class SimpleOrderCourseViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = SimpleOrderCourseSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        return OrderCourse.objects.select_related('order').filter(order__user=self.request.user)


class OrderCourseViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = SimpleOrderCourseSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        queryset = OrderCourse.objects.select_related('course')
        order_pk_str = self.kwargs.get('order_pk')
        try:
            order_pk_str = int(order_pk_str)
        except ValueError:
            return queryset.none()
        return queryset.filter(order=order_pk_str)


class OrderByDayView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        course_date = request.query_params.get('course_date', None)
        orders = Order.objects.filter(user=request.user)
        if course_date:
            orders = orders.filter(course__date=course_date)
        if orders:
            result = orders.order_by('course__date').filter(
                course__date__gte=(date.today() - timedelta(days=10))
            ).values(
                'course__date',
                'course__name',
                'course__week__number',
                'course__price'
            ).annotate(
                total=Sum('ordercourse__quantity'),
                ordercourses_id=ArrayAgg('ordercourse__id')
            )

            total = [
                {**{'weekday': Course.WEEKDAYS_NAMES[order['course__date'].weekday()]}, **order}
                for order in result
            ]
            return Response(
                {'status': 'ok', 'data': total}
            )
        return Response({'status': 'not_found', 'data': []})


class DownloadOrderToday(views.APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get(self, request):
        file_path = Order.objects.generate_orders_xls()
        with open(file_path, 'rb') as fd:
            response = HttpResponse(content=fd, content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(file_path)
        return response
