from menu.api import User
from menu.models import Company
from menu.serializers import CompanySerializer, UserSerializer
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAuthenticated


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)


class CompanyViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    filter_backends = (filters.DjangoFilterBackend,)
