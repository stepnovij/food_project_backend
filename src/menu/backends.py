from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


class NoPasswordUserBackend(ModelBackend):
    def authenticate(self, email, password=None, **kwargs):
        return User.objects.filter(email=email).first()
