import math
import os
import urllib
from datetime import datetime

from django.conf import settings
from django.core.management.base import BaseCommand

from utils.date import get_current_week
from utils.excel import XLSbook

from menu.models import (
    Course, OriginalCoursePrice, ProviderFileMenuSource, Week, Provider,
    COURSE_TYPES as COURSE_TYPES_ORDER
)

COURSE_TYPES = [
    'ПЕРВЫЕ БЛЮДА', 'ВТОРЫЕ БЛЮДА', 'САЛАТЫ ЗАПРАВЛЕННЫЕ  И ЗАКУСКИ',
    'САЛАТЫ НЕ ЗАПРАВЛЕННЫЕ', 'ЗАПРАВКИ К САЛАТАМ И СОУСЫ', 'ПИРОЖНОЕ', 'Прочее'
]

REVENUE = 1.1


def round_price(price, base=5):
    if price < 5:
        return round(price)
    return int(base * math.ceil(float(price) / base))


class Command(BaseCommand):

    def parse_xls(self):
        pass

    def parse_file(self):
        pass

    def read_worksheet(self, worksheet):
        rows = []
        for i, row in enumerate(range(worksheet.nrows)):
            r = []
            for j, col in enumerate(range(worksheet.ncols)):
                r.append(worksheet.cell_value(i, j))
            rows.append(r)
        return rows

    def download_file(self, url, file_path):
        response = urllib.request.urlopen(url)
        with open(file_path, 'wb') as output:
             output.write(response.read())
        return file_path

    def get_or_download(self, url, week_type, provider_id):
        week = self.get_week(week_type)
        filename = '{}_{}.xls'.format(provider_id, week.number)
        file_path = os.path.join(settings.MENU_FILES_DIR, filename)
        if os.path.exists(file_path):
            os.remove(file_path)
        file_path = self.download_file(url, file_path)
        ProviderFileMenuSource.objects.get_or_create(
            provider_id=provider_id,
            file_name=filename,
            week=week
        )
        return file_path

    def create_course(self, data, provider, course_date, week_number, update):
        if not Course.objects.filter(
            date=course_date,
            week=week_number,
            provider=provider,
            name=data[1].value,
        ).exists():
            course = Course.objects.create(
                week=week_number,
                name=data[1].value,
                date=course_date,
                price=round_price(data[3].value*1.1),
                weight=data[2].value,
                provider=provider,
                type=data[6],
                course_type_order=COURSE_TYPES_ORDER[data[6].lower()]
            )
            OriginalCoursePrice.objects.create(
                course=course,
                price=data[3].value
            )
        elif update:
            course = Course.objects.filter(
                week=week_number,
                date=course_date,
                provider=provider,
                name=data[1].value
            )
            course.update(
                price=round_price(data[3].value*1.1),
                weight=data[2].value,
                type=data[6]
            )
            OriginalCoursePrice.objects.get_or_create(
                course=course[0],
                price=data[3].value
            )

    def get_week(self, type_):
        week_number = get_current_week()
        if type_ == 'current':
            week, _ = Week.objects.get_or_create(number=week_number)
        else:
            week_number += 1
            week, _ = Week.objects.get_or_create(number=week_number)
        return week

    def get_menu_url(self, provider, type_):
        if type_ == 'current':
            return provider.current_week_menu_url
        return provider.next_week_menu_url

    def parse_date(self, date_str):
        date_str = date_str.split(' ')[0]
        try:
            result = datetime.strptime(date_str, '%d.%m.%y')
        except ValueError:
            result = None
        return result

    def find_course(self, course_menu):
        course = []
        course_type = None
        for row in course_menu:
            if row[0].value in COURSE_TYPES:
                course_type = row[0].value
            elif type(row[0].value) == float:
                row.append(course_type.lower())
                course.append(row)
        return course

    def add_arguments(self, parser):
        parser.add_argument(
            '--update',
            action='store_true',
            default=False,
            help='Update all courses',
        )

    def handle(self, *args, **options):
        provider = Provider.objects.filter(current_week_menu_url__isnull=False).first()
        if provider:
            for week_type in ('current', 'next'):
                file_path = self.get_or_download(
                    url=self.get_menu_url(provider, week_type),
                    week_type=week_type,
                    provider_id=provider.id
                )
                xls_book = XLSbook(file_path)
                xls_dict = xls_book.read_all_sheets()
                for course_date_str, course in xls_dict.items():
                    course_date = self.parse_date(course_date_str)
                    # Заканчиваем парсинг документ, если появилось другое название листа
                    # вместо %d.%m.%Y
                    if not course_date:
                        return
                    course = self.find_course(course)
                    for dish in course:
                        self.create_course(
                            dish, provider, course_date, self.get_week(week_type), options['update']
                        )
