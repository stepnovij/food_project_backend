from django.db import models


COURSE_TYPES = {
    'первые блюда': 1,
    'вторые блюда': 2,
    'салаты заправленные  и закуски': 3,
    'салаты не заправленные': 4,
    'заправки к салатам и соусы': 5,
    'пирожное': 6,
    'прочее': 7
}


class Provider(models.Model):
    name = models.CharField(max_length=1024)
    url = models.URLField()
    current_week_menu_url = models.URLField()
    next_week_menu_url = models.URLField(null=True)

    def __str__(self):
        return self.name


class Week(models.Model):
    number = models.PositiveIntegerField()

    def __str__(self):
        return str(self.number)


class ProviderFileMenuSource(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)
    file_name = models.CharField(max_length=1024)
    week = models.ForeignKey(Week, on_delete=models.PROTECT)

    def __str__(self):
        return '{} {} {}'.format(self.provider, self.file_name, self.week)


class Course(models.Model):
    WEEKDAYS_NAMES = ('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье')
    #TODO: Types should be done with integers
    week = models.ForeignKey(
        Week,
        on_delete=models.PROTECT
    )
    date = models.DateField()
    name = models.CharField(max_length=1024)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    weight = models.CharField(max_length=1024)
    provider = models.ForeignKey(Provider)
    type = models.CharField(max_length=256)
    course_type_order = models.PositiveIntegerField()

    @property
    def weekday(self):
        return self.WEEKDAYS_NAMES[self.date.weekday()]

    class Meta:
        unique_together = ('provider', 'week', 'date', 'name')
        ordering = ['date', 'type']

    def __str__(self):
        return '{} {} {}'.format(self.name, self.date, self.week.number)


class OriginalCoursePrice(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=6, decimal_places=2)
