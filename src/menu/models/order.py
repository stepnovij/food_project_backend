import os
from datetime import date, timedelta
from decimal import Decimal

import xlwt
from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from menu.models.course import Course, ProviderFileMenuSource, Week
from utils.excel import XLSbook


def get_order_date():
    if date.today().weekday() >= 4:
        return date.today() + timedelta(days=(7 - date.today().weekday()))
    else:
        return date.today() + timedelta(days=1)


class OrderQuerySet(models.query.QuerySet):
    def delete(self):
        for obj in self.iterator():
            obj.delete()
        super().delete()


class OrderManager(models.Manager):
    def get_queryset(self):
        return OrderQuerySet(self.model, using=self._db)

    def delete(self):
        return self.get_queryset().delete()

    def get_all_orders_for_date(self, date_=date.today(), course__name=None):
        if not course__name:
            return self.model.objects.select_related('course').filter(
                course__date=date_,
            ).values('course__date', 'course__name', 'week__number').annotate(
                total=models.Sum('ordercourse__quantity')
            )
        else:
            return self.model.objects.select_related('course').filter(
                course__date=date_,
                course__name=course__name,
            ).values('course__date', 'course__name', 'week__number').annotate(
                total=models.Sum('ordercourse__quantity')
            )

    @staticmethod
    def user_course_query():
        from menu.models.user import OrderUser
        date_ = date.today()
        sheet_name = str(date_)
        workbook = xlwt.Workbook()
        sheet = workbook.add_sheet(sheet_name)
        user_index = 0
        for user in OrderUser.objects.filter(order__ordercourse__course__date
                                             =date_).distinct('email'):
            result = OrderUser.objects.filter(email=user.email,
                                              order__ordercourse__course__date=date_).values_list(
                                              'order__ordercourse__course__name',
                                              'order__ordercourse__quantity')

            sheet.write(0, user_index, str(user).split('@')[0])
            for item_index, item in enumerate(result):
                if item:
                    sheet.write(item_index+1, user_index, str(item[0]))
                    sheet.write(item_index+1, user_index+1, str(item[1]))
            user_index += 2
        file_path = os.path.join(settings.MENU_FILES_DIR, '{}.xls'.format(date_))
        workbook.save(file_path)

    def generate_orders_xls(self):
        date_ = get_order_date()
        week_number = date_.isocalendar()[1]
        file_source = ProviderFileMenuSource.objects.get(week__number=week_number)
        file_path = os.path.join(settings.MENU_FILES_DIR, file_source.file_name)
        xls_book = XLSbook(file_path)
        xls_dict = xls_book.read_all_sheets()
        for course_date_str, courses in xls_dict.items():
            # format: 30.11.16 Среда
            course_date, _ = course_date_str.split(' ')
            if course_date != date_.strftime('%d.%m.%y'):
                continue
            workbook = xlwt.Workbook()
            sheet = workbook.add_sheet(course_date_str)
            index_of_quantity = 0
            index_of_total_sum = 0
            index_of_name = 0
            for index, value in enumerate(courses):
                course = ''
                for index_row, value_row in enumerate(value):
                    if index_of_quantity == 0 and value_row.value == 'Кол-во':
                        index_of_quantity = index_row
                    if index_of_total_sum == 0 and value_row.value == 'Сумма(р)':
                        index_of_total_sum = index_row
                    if index_of_name == 0 and value_row.value == 'Наименование':
                        index_of_name = index_row
                    if index != 1 and index_of_name == index_row and value_row.value:
                        course = value_row.value
                    if course and index_row == index_of_quantity:
                        result = self.model.objects.get_all_orders_for_date(date_, course)
                        if len(result) > 0:
                            sheet.write(index, index_row, result[0]['total'])
                    else:
                        sheet.write(index, index_row, value_row.value)
            file_path = os.path.join(settings.MENU_FILES_DIR, '{}.xls'.format(date_))
            workbook.save(file_path)
            return file_path


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    course = models.ManyToManyField(Course, through='OrderCourse')
    week = models.ForeignKey(Week)
    is_payed = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    objects = OrderManager()

    def __str__(self):
        return '{} {} {}'.format(self.id, self.week, self.user)

    def delete(self, *args, **kwargs):
        if self.is_payed:
            balance = UserMoneyBalance.objects.filter(user=self.user)
            if balance.exists():
                balance.update(
                    residue=(balance.first().residue + self.total_sum)
                )
        super().delete(*args, **kwargs)

    @property
    def total_sum(self):
        return sum(
            order_course.quantity*order_course.course.price
            for order_course in self.ordercourse_set.all()
        )


class OrderCourse(models.Model):
    order = models.ForeignKey(Order)
    course = models.ForeignKey(Course)
    quantity = models.PositiveIntegerField()

    def _check_qunatity_ordercourse(self, order_course, delta_quantity):
        balance = UserMoneyBalance.objects.last()
        order = Order.objects.get(id=self.order.id)
        if delta_quantity > 0:
            if (
                balance.residue >= delta_quantity * order_course.course.price
                and order.is_payed
            ):
                balance.residue -= delta_quantity * order_course.course.price
                balance.save()
            elif (
                balance.residue < delta_quantity * order_course.course.price
                and order.is_payed
            ):
                balance.residue += order_course.order.total_sum
                balance.save()
                order.payment.delete()
        else:
            if order.is_payed:
                balance.residue += (order_course.quantity - self.quantity) * self.course.price
                balance.save()
            else:
                if (
                    (order.total_sum + delta_quantity*order_course.course.price) <
                            balance.residue
                ):
                    # order.is_payed = True
                    # order.save()
                    order_payment = OrderPayment(
                        payed_order=order,
                        payment=UserMoneyPayment.objects.all().last(),
                    )
                    order_payment.save()
                    balance.residue -= order.total_sum + delta_quantity*order_course.course.price
                    balance.save()

    def save(self, *args, **kwargs):
        is_old = self.id
        if is_old:
            # Проверка на изменение количества:
            order_course = OrderCourse.objects.get(id=self.id)
            if order_course and order_course.quantity != self.quantity:
                balance = self.order.user.usermoneybalance
                delta_quantity = self.quantity - order_course.quantity
                self._check_qunatity_ordercourse(order_course, delta_quantity)
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        order = Order.objects.get(id=self.order.id)
        balance = UserMoneyBalance.objects.last()
        if order.is_payed:
            balance.residue = order.user.usermoneybalance.residue
            balance.residue += self.course.price*self.quantity
            balance.save()
        super().delete(*args, **kwargs)

    class Meta:
        ordering = ['order', 'course__date']
        unique_together = ('order', 'course')

    def __str__(self):
        return '{} {}'.format(self.course, self.quantity)


class UserMoneyPayment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    money = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    payed_at = models.DateField()

    def __str__(self):
        return '{}: {}'.format(self.user.email, self.money)

    def _pay_not_payed_orders(self, residue):
        not_payed_orders = Order.objects.filter(
            user=self.user,
            is_payed=False
        ).order_by('id')
        for order in not_payed_orders:
            if order.total_sum > residue:
                return residue
            order_payment = OrderPayment(
                payed_order=order,
                payment=self,
            )
            order_payment.save()
            residue = residue - order.total_sum
        return residue

    def _cancel_payed_orders(self, delta):
        orders = Order.objects.filter(is_payed=True).order_by('-id').iterator()
        total_sum = 0
        for order in orders:
            OrderPayment.objects.filter(payed_order=order).last().delete()
            total_sum += order.total_sum
            if total_sum > abs(delta):
                return total_sum
        return total_sum

    def save(self, *args, **kwargs):
        is_old = self
        balance = UserMoneyBalance.objects.filter(user=self.user).last()
        delta = self.money
        if not is_old.id:
            residue = self.money
            residue = residue + balance.residue
        else:
            previous_payment = UserMoneyPayment.objects.get(id=is_old.id)
            # Вычитаем старую сумму и добавляем новую
            delta = self.money - previous_payment.money
            residue = balance.residue + delta
        super().save(*args, **kwargs)
        # UserMoneyBalance просчет остатка для увелечени и уменьшения платежа:
        if delta > 0:
            residue = self._pay_not_payed_orders(residue)
        else:
            residue += self._cancel_payed_orders(delta)
        balance.residue = residue
        balance.save(update_fields=['residue'], force_update=True)


class UserMoneyBalance(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    residue = models.DecimalField(
        default=0, max_digits=10, decimal_places=2,
        validators=[MinValueValidator(Decimal('0.00'))]
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Остаток средств пользователя'
        verbose_name_plural = 'Остатки средств пользователей'

    def __str__(self):
        return '{} {}'.format(self.user.email, self.residue)

    @property
    def balance(self):
        total_sum = 0
        not_payed_user_orders = Order.objects.filter(user=self.user, is_payed=False)
        for order in not_payed_user_orders:
            total_sum += order.total_sum
        return self.residue - total_sum


class OrderPayment(models.Model):
    payment = models.ForeignKey(UserMoneyPayment)
    payed_order = models.ForeignKey(Order)
    balance_payment = models.ForeignKey(UserMoneyBalance, null=True, blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        order = Order.objects.get(id=self.payed_order.id)
        order.is_payed = True
        order.save()

    def delete(self, *args, **kwargs):
        order = Order.objects.get(id=self.payed_order.id)
        order.is_payed = False
        order.save()
        super().delete(*args, **kwargs)
