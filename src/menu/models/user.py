import binascii
import os

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone

from menu.models.order import UserMoneyBalance


def _generate_code():
    return binascii.hexlify(os.urandom(20))


class Company(models.Model):
    name = models.CharField(max_length=1024)
    # TODO:  по-хорошему надо разбить на город, улицу и т.д.
    address = models.TextField()
    is_active = models.BooleanField(default=True)
    email_domain = models.CharField(max_length=32)

    def __str__(self):
        return '{}: {}, {}'.format(self.name, self.address, self.email_domain)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class OrderUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email address', unique=True, blank=True)
    date_joined = models.DateTimeField('date joined', default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    company = models.ForeignKey(Company, null=True)
    objects = UserManager()

    USERNAME_FIELD = 'email'

    @property
    def info(self):
        return {
            'email': self.email,
            'company': self.company
        }

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def save(self, *args, **kwargs):
        is_old = self.id
        super().save(*args, **kwargs)
        user = OrderUser.objects.get(id=self.id)
        if not is_old:
            balance = UserMoneyBalance(
                user=user,
                residue=0,
            )
            balance.save()


class SignupCodeManager(models.Manager):
    def create_signup_code(self, user, ipaddr):
        code = _generate_code()
        signup_code = self.create(user=user, code=code, ipaddr=ipaddr)

        return signup_code

    def set_user_is_verified(self, code):
        try:
            signup_code = SignupCode.objects.get(code=code)
            signup_code.user.is_verified = True
            signup_code.user.save()
            return True
        except SignupCode.DoesNotExist:
            pass

        return False


class SignupCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    code = models.CharField(max_length=40, primary_key=True)
    ipaddr = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    objects = SignupCodeManager()

    def send_signup_email(self):
        ctxt = {
            'email': self.user.email,
            'code': self.code,
            'site': '62.109.14.234'
        }
        send_multi_format_email('signup_email', ctxt, target_email=self.user.email)


def send_multi_format_email(template_prefix, template_ctxt, target_email):
    subject_file = '%s_subject.txt' % template_prefix
    txt_file = '%s.txt' % template_prefix
    html_file = '%s.html' % template_prefix
    subject = render_to_string(subject_file).strip()
    from_email = settings.DEFAULT_EMAIL_FROM
    to = target_email
    text_content = render_to_string(txt_file, template_ctxt)
    html_content = render_to_string(html_file, template_ctxt)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, 'text/html')
    msg.send()
