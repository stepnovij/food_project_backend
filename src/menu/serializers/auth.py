from rest_framework import serializers


class CredentialsSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=50, required=True)
    password = serializers.CharField(min_length=5, required=True)

    def validate(self, attrs):
        email = attrs.get('email')
        if '@' in email:
            username, domain = email.split('@')
            if not domain == 'rambler-co.ru':
                raise serializers.ValidationError('Только почта с домена @rambler-co.ru '
                                                  'может быть использована для регистрации')
            return attrs
        raise serializers.ValidationError('Для входа необходимо использовать почту вида:'
                                          ' username@rambler-co.ru ')
