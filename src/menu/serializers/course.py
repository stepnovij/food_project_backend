from menu.models import Course, Provider, Week
from rest_framework import serializers


class WeekSerializer(serializers.ModelSerializer):
    class Meta:
        model = Week


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider


class CourseSerializer(serializers.ModelSerializer):
    week = WeekSerializer(read_only=True)
    course_type = serializers.SerializerMethodField()

    class Meta:
        model = Course
        fields = (
            'id', 'week', 'date', 'weekday', 'name', 'price', 'weight', 'provider', 'course_type'
        )

    def get_course_type(self, instance):
        return instance.type.title()
