from menu.models.order import Order, OrderCourse, OrderPayment
from menu.serializers.course import CourseSerializer, WeekSerializer
from rest_framework import serializers


class SimpleOrderCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderCourse


class OrderCourseSerializer(serializers.ModelSerializer):
    course = CourseSerializer()

    class Meta:
        model = OrderCourse
        fields = ('quantity', 'course',)


class OrderSerializer(serializers.ModelSerializer):
    ordercourse_set = OrderCourseSerializer(
        required=False,
        many=True
    )
    week = WeekSerializer(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'user', 'ordercourse_set', 'is_payed', 'week', 'created_at')
        read_only_fields = ('order', 'user')

    def create(self, validated_data):
        courses = self.initial_data.pop('course')
        week = self.initial_data.pop('week')
        quantity = self.initial_data.pop('quantity')
        # TODO: rewrite this part
        validated_data['week_id'] = week['id']
        order = Order.objects.create(**validated_data)
        [
            OrderCourse.objects.create(
                order=order,
                course_id=course,
                quantity=quantity[index],
            ) for index, course in enumerate(courses)
        ]
        balance = order.user.usermoneybalance
        payments = order.user.usermoneypayment_set.all()
        total_sum = order.total_sum
        if balance.residue >= total_sum:
            residue = balance.residue - total_sum
            balance.residue = residue
            balance.save()
            payment = None
            if payments.exists():
                payment = payments.last()
            order_payment = OrderPayment(
                payed_order=order,
                payment=payment,
                balance_payment=balance
            )
            order_payment.save()
        return order
