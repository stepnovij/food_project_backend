from django.contrib.auth import get_user_model
from menu.models import Company, Course, UserMoneyBalance
from rest_framework import serializers

User = get_user_model()


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company


class SimpleCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course


class UserSerializer(serializers.ModelSerializer):
    balance = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'id', 'email', 'date_joined', 'company', 'balance'
        )

    def get_balance(self, object):
        balance = UserMoneyBalance.objects.filter(user=object)
        if balance.exists():
            return balance.last().balance