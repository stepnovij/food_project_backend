import json
from django_dynamic_fixture import G

from datetime import date, datetime, timedelta

from django.contrib.auth import get_user_model
from django.test import testcases
from django.urls import reverse

import menu.models as menu_models
from utils.date import get_current_week

User = get_user_model()


class RecruitADAttributesTestCase(testcases.TestCase):
    def setUp(self):
        self.user = G(User, email='test@rambler-co.ru', password='123456')
        self.provider = G(menu_models.Provider)
        self.date = date.today() + timedelta(days=2)
        self.week = G(menu_models.Week, number=get_current_week())
        self.course1 = G(menu_models.Course,
                         provider=self.provider,
                         price=100.00,
                         week=self.week,
                         date=self.date)
        self.course2 = G(menu_models.Course,
                         provider=self.provider,
                         price=120.00,
                         week=self.week,
                         date=self.date)
        self.course3 = G(menu_models.Course,
                         provider=self.provider,
                         price=110.00,
                         week=self.week,
                         date=self.date)
        self.order1 = G(menu_models.Order, user=self.user, is_payed=False, week=self.week)
        self.order2 = G(menu_models.Order, user=self.user, is_payed=False, week=self.week)

    def test_creating_paying_order(self):
        # В самом начале баланс равен 0
        self.assertEqual(self.user.usermoneybalance.balance, 0)
        order1_course1 = G(menu_models.OrderCourse, course=self.course1, order=self.order1,
                          quantity=5)
        order1_course2 = G(menu_models.OrderCourse, course=self.course2, order=self.order1,
                          quantity=3)
        order2_course2 = G(menu_models.OrderCourse, course=self.course3, order=self.order2,
                          quantity=3)
        all_orders_sum = self.order1.total_sum + self.order2.total_sum
        # При создание ордера баланс уходит в минус:
        self.assertEqual(
            -all_orders_sum, self.user.usermoneybalance.balance
        )
        self.assertEqual(menu_models.OrderPayment.objects.count(), 0)
        # Добавление оплаты:
        payment1 = menu_models.UserMoneyPayment.objects.create(
            user=self.user, money=300, payed_at=date.today()
        )
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment1.money - all_orders_sum, balance.balance
        )
        # Добавление оплаты еще раз:
        payment2 = menu_models.UserMoneyPayment.objects.create(
            user=self.user, money=500, payed_at=date.today()
        )
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment2.money + payment1.money - all_orders_sum, balance.balance
        )
        # Добавление оплаты еще раз:
        payment3 = menu_models.UserMoneyPayment.objects.create(
            user=self.user, money=300, payed_at=date.today()
        )
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment3.money + payment2.money + payment1.money - all_orders_sum,
            balance.balance
        )
        self.assertTrue(menu_models.Order.objects.first().is_payed)
        self.assertEqual(menu_models.OrderPayment.objects.count(), 1)
        # Удаление части блюд в заказе 2:
        ordercourse = self.order2.ordercourse_set.all().first()
        ordercourse.quantity = 1
        ordercourse.save()
        all_orders_sum_next = sum([order.total_sum for order in menu_models.Order.objects.all()])
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment3.money + payment2.money + payment1.money - all_orders_sum_next,
            balance.balance, all_orders_sum_next
        )
        #  Теперь все заказы должны быть оплачены:
        self.assertTrue(menu_models.Order.objects.all()[0].is_payed)
        self.assertTrue(menu_models.Order.objects.all()[1].is_payed)
        self.assertEqual(menu_models.OrderPayment.objects.count(), 2)
        # Удаление части блюд в заказе 1:
        ordercourse = self.order1.ordercourse_set.all().first()
        ordercourse.quantity = 1
        ordercourse.save()
        all_orders_sum_next = sum([order.total_sum for order in menu_models.Order.objects.all()])
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment3.money + payment2.money + payment1.money - all_orders_sum_next,
            balance.balance, all_orders_sum_next
        )
        self.assertTrue(menu_models.Order.objects.all()[0].is_payed)
        self.assertTrue(menu_models.Order.objects.all()[1].is_payed)
        self.assertEqual(menu_models.OrderPayment.objects.count(), 2)
        # Удаление ordercourse совсем:
        ordercourse = self.order1.ordercourse_set.all().first()
        ordercourse.delete()
        all_orders_sum_next = sum([order.total_sum for order in menu_models.Order.objects.all()])
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment3.money + payment2.money + payment1.money - all_orders_sum_next,
            balance.balance, all_orders_sum_next
        )
        self.assertTrue(menu_models.Order.objects.all()[0].is_payed)
        self.assertTrue(menu_models.Order.objects.all()[1].is_payed)
        # Изменение payment
        payment2.money = 5
        payment2.save()
        all_orders_sum_next = sum([order.total_sum for order in menu_models.Order.objects.all()])
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment3.money + payment2.money + payment1.money - all_orders_sum_next,
            balance.balance, all_orders_sum_next
        )
        self.assertFalse(menu_models.Order.objects.all()[0].is_payed)
        self.assertFalse(menu_models.Order.objects.all()[1].is_payed)
        self.assertEqual(menu_models.OrderPayment.objects.count(), 0)
        # Добавляем еще одну оплату:
        payment4 = menu_models.UserMoneyPayment.objects.create(
            user=self.user, money=300, payed_at=date.today()
        )
        all_orders_sum_next = sum([order.total_sum for order in menu_models.Order.objects.all()])
        balance = menu_models.UserMoneyBalance.objects.filter(user=self.user).first()
        self.assertEqual(
            payment4.money + payment3.money + payment2.money + payment1.money - all_orders_sum_next,
            balance.balance, all_orders_sum_next
        )
        self.assertTrue(menu_models.Order.objects.all()[0].is_payed)
        self.assertTrue(menu_models.Order.objects.all()[1].is_payed)

    def test_create_order_by_api(self):
        self.client.login(email='test@rambler-co.ru', password='123456')
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('course-list'), data={'week': 'current'})
        self.assertEqual(response.status_code, 200)
        data2 = {
            'week': {'id': self.week.id},
            'course': [response.data[0]['id'], response.data[1]['id']],
            'is_payed': False,
            'quantity': ['1', '1']
        }
        response = self.client.post(reverse('order-list'), json.dumps(data2),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(menu_models.Order.objects.count(), 3)
        self.assertEqual(menu_models.Order.objects.all()[0].is_payed, False)
        self.assertEqual(menu_models.Order.objects.all()[1].is_payed, False)
        self.assertEqual(menu_models.Order.objects.all()[2].is_payed, False)
        menu_models.UserMoneyPayment.objects.create(user=self.user, money=2000,
                                                    payed_at=date.today())
        self.assertEqual(menu_models.Order.objects.count(), 3)
        self.assertEqual(menu_models.Order.objects.all()[0].is_payed, True)
        self.assertEqual(menu_models.Order.objects.all()[1].is_payed, True)
        self.assertEqual(menu_models.Order.objects.all()[2].is_payed, True)
