from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from django.conf.urls import include, url

from menu.api import CourseViewSet, ProviderViewSet, WeekView
from menu.api.auth import APILogin, APILogout, Signup, SignupVerify
from menu.api.order import (
    OrderViewSet, OrderByDayView, OrderCourseViewSet, SimpleOrderCourseViewSet
)
from menu.api.user import CompanyViewSet, UserViewSet


router = DefaultRouter()
router.register(r'course', CourseViewSet)
router.register(r'provider', ProviderViewSet)
router.register(r'company', CompanyViewSet)
router.register(r'user', UserViewSet, base_name='user')
router.register(r'order', OrderViewSet, base_name='order')
router.register(r'ordercourse_simple', SimpleOrderCourseViewSet, base_name='order')
ordercourses_router = routers.NestedSimpleRouter(router, r'order', lookup='order')
ordercourses_router.register(r'ordercourse', OrderCourseViewSet, base_name='order-ordercourse')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(ordercourses_router.urls)),
    url(r'^login/$', APILogin.as_view(), name='login'),
    url(r'^logout/$', APILogout.as_view(), name='logout'),
    url(r'^signup/$', Signup.as_view(), name='signup'),
    url(r'^signup/verify/$', SignupVerify.as_view(), name='signup-verify'),
    url(r'^week/$', WeekView.as_view(), name='week'),
    url(r'^order_by_day/$', OrderByDayView.as_view(), name='order_by_day'),
]
