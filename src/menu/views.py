from django.shortcuts import render


def main(request):
    if request.user.is_authenticated():
        serialized_user = request.user.info
    else:
        serialized_user = None
    context = {'current_user': serialized_user}
    return render(request, template_name='index.html', context=context)
