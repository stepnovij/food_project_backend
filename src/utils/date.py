from datetime import date


def get_current_week(date_=date.today()):
    return date_.isocalendar()[1]
