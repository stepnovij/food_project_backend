import xlrd


class XLSbook:
    def __init__(self, file_path):
        self.workbook = xlrd.open_workbook(file_path)

    def read_worksheet(self, sheet):
        array_sheet = []
        for row in range(0, sheet.nrows):
            array_sheet.append(sheet.row(row))
        return array_sheet

    def read_all_sheets(self):
        book = {}
        for sheet_number in range(0, self.workbook.nsheets):
            sheet = self.workbook.sheet_by_index(sheet_number)
            book[sheet.name] = (self.read_worksheet(sheet))
        return book
